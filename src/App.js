import './App.css';
import Navbar from './components/Navbar';
import { Route, Routes } from 'react-router-dom';
import { Home, Codebase, Discord, NoPage, Play, Rules, Wiki, Donate } from "./pages/Pages";


function App() {
  return (
    <>
      <div className="content min-h-screen bg-repeat-space bg-black bg-fixed bg-[url('/src/assets/background_image.jpg')] overflow-x-clip text-white font-mono">

      <table className="w-screen border-transparent">
        <tr className="h-24">
          <td rowspan="2" className="w-0 xl:w-15screen ">
          </td>
          <td className="w-70screen border-transparent border-dashed">

            <Navbar />
          </td>
          <td rowspan="2" className="w-0  xl:w-15screen ">
          </td>
        </tr>
        <tr className="h-fit">
          <td className="content bg-slate-900/70 drop-shadow-md rounded-2xl">

            <Routes>
              <Route path="/">
                <Route index element={<Home />} />
                <Route path="play" element={<Play />} />
                <Route path="rules" element={<Rules />} />
                <Route path="wiki" element={<Wiki />} />
                <Route path="discord" element={<Discord />} />
                <Route path="codebase" element={<Codebase />} />
                <Route path="donate" element={<Donate />} />
                <Route path="*" element={<NoPage />} />
              </Route>
            </Routes>

          </td>
        </tr>
      </table>
    </div>
    </>
  );
}

export default App;
