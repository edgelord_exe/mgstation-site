import { useEffect } from "react"


export function Home() {
  return (
    <>
      <div className="h-screen w-auto">
        <div className="align-top grid place-items-center h-64 text-5xl font-bold">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
          Welcome to Merchant Station 13
          </div>
        </div>
        <div className="align-top grid place-items-center text-lg text-center font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            Merchant Station 13, is an LRP gaming community centered around our own TGstation fork from late 2021. <br/>
            We are dedicated to providing the most exciting, autistic, and fun way to experience the chaos of LRP.<br/>
            Our goal is to bring back the fun of chaotic LRP with as few limits as possible. <br/>
            To accomplish this we have ported many features from Hippie and added a ton of our own content, to learn more checkout our wiki. <br/>
            Currently our server are on fire, so we encourage you to donate, so that we can enrich LRP experience even further. <br/>
          </div>
        </div>
      </div>
    </>
  )
}

export function Play() {

  useEffect(() => {
    window.location.href = "byond://benito.merchantstation.net:1337";
  },[]);

  return (
    <>
      <div className="align-top grid place-items-center text-lg text-center font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            Sending...
          </div>
      </div>
    </>
  )
}

export function Rules() {
  return (
    <>
       <div className="h-fit w-auto">
        <div className="align-top grid place-items-center h-40 text-5xl font-bold">
          <div className="grid place-items-center margin border-8 p-6 rounded-lg bg-black" >
            Merchant Station Rules
          </div>
        </div>
        <div className="align-top grid place-items-center text-lg text-justify font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            <ol>
                <li><div className="font-bold text-center" >0. Admins have the final say. </div>
                This mostly applies to banning players who are an active, serious detriment to the server. If the admins notice that a player is here only to ruin rounds/powergame/valid hunt/toe the line,they reserve the right to GET THAT ASS BANNED.
                Admins are probably human too, Ahelping for stupid reasons or to just whine and insult the admin will get you btfo’d, in some way, up to the discretion of the admin.</li><br/>
                <li><div className="font-bold text-center" >1. Don’t be a dick </div>
                Quite simple to follow, while we allow most forms of speech, griefing, being an ass to admins and other players OOC is not fun. Banter is fine, but admins reserve the right to ban people that are overall detriment to the server.
                Forced ERP and other disturbing behaviour will get you permanently banned instantly.</li><br/>
                <li><div className="font-bold text-center" >2. This is an LRP server</div>
                We aren’t HRP, nor is RP truly required (look 2.1). If you want to RP it is fine and even encouraged to play out your character.
                Admins are actively trying to increase the overall fun of the round, roleplaying as a fun character or attempting a good gimmick will usually make admins more receptive to your ideas and actions.</li><br/>
                <div className="text-base">
                <li><div className="font-bold text-center" >2.1. Minimal RP requirements</div>
                    ⊚Netspeak should be left to a minimum, put some effort into your character for God’s sake.<br/>
                    ⊚Metagangs are not allowed, permabannable offense.<br/>
    ⊚Metafriends are allowed, but not to the point of actual metagaming. No giving eachother AA and blowies in the bathrooms every round, folks.<br/>
                    ⊚Metagrudges is mostly disallowed, with the exception of rivalries or player-led moves against known bad players/poopy powergamers.<br/>
                    ⊚Revealing important information about the round in OOC or on other modes of communications is banned, look 2.3 for a single exception to this rule.
                    ⊚Security is bound by Captain’s whim and Space Law. The Captain is to blame for any actions ordered by him and executed by security. The blame for actions commited by security who were ordered by captain are shifted on him.<br/>
                    ⊚Don’t suicide right after the round starts, if you want to leave the round close to roundstart notify admins instead.<br/>
                    ⊚ERP and reading certain copypastas like Woody’s Got Wood over comms will make you valid to the entire server, players and admins. Talking about ahelping ingame will get a bored admin to abuse you in whatever way seems the funniest.<br/>
                </li><br/>
                <li><div className="font-bold text-center" >2.2. Escalation</div>
                  You are always allowed to fight back and defend yourself. You can either defend yourself, or Ahelp. Not both.
                  An eye for an eye and a tooth for a tooth is the rule of the game. You can kill someone for stealing important goods, or constantly harrassing you, but not for just disarming you a few times or throwing a plant at you.
                </li><br/>
                <li><div className="font-bold text-center" >2.3. New player assistance</div>
                  If a new player wants to get instructed by an older player using Discord or other such methods, they must notify the admins beforehand. After permission is given, they can teach the game. If this is used to save each other or name the antags, you will get your ass banned.
                </li><br/>
                </div>
                <li><div className="font-bold text-center" >3. Antagonists can safely ignore most rules</div>
                  Except for the metacomm, metagang, forced ERP and metafriend rule, the rules don’t apply to them.
                  Team antagonists must respect rule 1 in regards to their teammates.
                  Ignoring team objectives will get you banned from team antags if it ends up ruining the fun for your teammates.
                </li><br/>
                <div className="text-base">
                <li><div className="font-bold text-center" >3.1. Murderboning</div>
                  Murderboning is allowed, as is prolonging the round for no reason, but it allows admins to take action against you such as: sending an ERT / Interns / A deathsquad / Embedding earrape in your chatbox.
                </li><br/>
                </div>
                <li><div className="font-bold text-center" >4. Powergaming</div>
                  Powergaming is allowed, but if your actions cause the round to be worse, then it will be treated as griefing, allowing the admin to deal with you ingame. If the unfun powergaming is kept up for multiple rounds, meta actions such as job bans and such will be taken instead. The difference between stealing the armoury and deepfry bombing it is that one makes you stronger while the other is funny and doesn’t make you better suited to kill people.
                </li><br/>
                <div className="text-base">
                <li><div className="font-bold text-center" >4.1. Self-converting is not allowed.</div>
                  Playing like shit just to get “conveniently” converted by antags is not allowed. You could get antag banned or worse.
                </li><br/>

                </div>
                <li><div className="font-bold text-center" >5. You must be 18 or over to play this game</div>
                  To play this server you need to be at least 18 years old. We aren’t going to investigate anyone’s age, but if they are retarded enough to straight up tell us, they get banned (if underaged). Don’t ask, don’t tell.
                </li><br/>
            </ol>
          </div>
        </div>
      </div>

    </>
  )
}

export function Wiki() {
  return (
    <>
      <div className="align-top grid place-items-center text-5xl text-center font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            Work in progress come back soon!
          </div>
      </div>
    </>
  )
}

export function Discord() {

  useEffect(() => {
    window.location.href = "https://discord.gg/2YYfU3ZanD";
  },[]);

  return (
    <>
      <div className="align-top grid place-items-center text-lg text-center font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            Sending...
          </div>
      </div>
    </>
  )
}

export function Codebase() {

  useEffect(() => {
    window.location.href = "https://github.com/The-Merchants-Guild/Merchant-Station-13";
  },[]);

  return (
    <>
      <div className="align-top grid place-items-center text-lg text-center font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            Sending...
          </div>
      </div>
    </>
  )
}

export function Donate() {

  useEffect(() => {
    window.location.href = "https://ko-fi.com/merchantstation";
  },[]);

  return (
    <>
      <div className="align-top grid place-items-center text-lg text-center font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            Sending...
          </div>
      </div>
    </>
  )
}

export function NoPage() {
  return (
    <>
      <div className="align-top grid place-items-center text-5xl text-center font-light leading-loose">
          <div className="grid place-items-center margin border-8 p-8 rounded-lg bg-black" >
            No page found!
          </div>
      </div>
    </>
  )
}
