import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../assets/logo.png'


function Navbar(){
  return (
	<nav className="flex xl:justify-evenly content-center h-32 bg-black m-0 space-x-0 space-y-0 rounded-2xl drop-shadow-lg shadow-md text-white relative font-mono" >
    <div className="flex flex-row place-content-start justify-items-start justify-start">
      <div className="flex h-32 w-32 items-center justify-start">
        <img className="inline " src={logo} alt="" ></img>
      </div>
      <div className="flex items-center justify-start">
      <Link to='/' className='text-left text-2xl hover:font-black content-left'> Merchant Station 13 </Link>
    	</div>
    </div>
    <div className="flex items-center justify-center text-center shadow-white">
		  <Link to='/play'					 			className='p-4 font-light hover:font-bold hover:align-text-top'>PLAY</Link>
		  <Link to='/rules' 		className='p-4 font-light hover:font-bold hover:align-text-top'>RULES</Link>
		  <Link to='/wiki' 			className='p-4 font-light hover:font-bold hover:align-text-top'>WIKI</Link>
		  <Link to='/discord' 	className='p-4 font-light hover:font-bold hover:align-text-top'>DISCORD</Link>
		  <Link to='/codebase' 	className='p-4 font-light hover:font-bold hover:align-text-top'>CODEBASE</Link>
		  <Link to='/donate' 		className='p-4 font-light hover:font-bold hover:align-text-top'>DONATE</Link>
     </div>
	</nav>
  )
}

export default Navbar;
