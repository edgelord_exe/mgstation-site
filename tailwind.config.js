module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      width: {
        '15screen': '15vw',
        '70screen': '70vw'
      }
    },
  },
}
